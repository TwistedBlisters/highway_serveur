package tn.esprit.pidev.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.pidev.entity.User;
@Remote
public interface UserServiceRemote {
	public  int ajouterUser(User user);
	public void deleteUserByid(int UserId);
	public User getUserById(int userId);
	public void UpdateUser(String adress,String password,String lastName,String firstName,String email,int UserId);
	public List<User> getUsers();

	
	
	
	

}
