package tn.esprit.pidev.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.pidev.entity.User;

@Stateless
public  class UserService implements UserServiceRemote{

	@PersistenceContext(unitName="pidev-ejb")
	EntityManager em;
	public int ajouterUser(User user) {
	
		em.persist(user);
		return user.getId();
	}

	@Override
	public void deleteUserByid(int UserId) {
		em.remove(em.find(User.class, UserId));
		
	}

	@Override
	public User getUserById(int userId) {
	TypedQuery<User> query=em.createQuery("SELECT u FROM User u WHERE u.id=:userId", User.class);
			query.setParameter("userId",userId);
		return query.getSingleResult();
	}
	@Override
	public List<User> getUsers() {
		TypedQuery<User> query=em.createQuery("SELECT u FROM User u",User.class);
	return query.getResultList();
	}

	@Override
	public void UpdateUser(String adress, String password, String lastName, String firstName, String email,
			int UserId) {
		User user=em.find(User.class, UserId);
		user.setAddress(adress);
		user.setPwd(password);
		user.setLast_name(lastName);
		user.setFirst_name(firstName);
		user.setMail(email);
		
		
	}

	


	
}
