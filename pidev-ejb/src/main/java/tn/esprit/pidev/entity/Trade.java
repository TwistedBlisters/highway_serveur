package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
@Entity
public class Trade extends Option implements Serializable{


private Date action_date;
@ManyToMany
private List<CurrencyAccount>currency_account;


public Date getAction_date() {
	return action_date;
}
public void setAction_date(Date action_date) {
	this.action_date = action_date;
}
public List<CurrencyAccount> getCurrency_account() {
	return currency_account;
}
public void setCurrency_account(List<CurrencyAccount> currency_account) {
	this.currency_account = currency_account;
}


}
