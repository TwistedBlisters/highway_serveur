package tn.esprit.pidev.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
@Entity

public class Trader extends User implements Serializable{
	
private boolean active;
@OneToMany
private List<Option>options;
@ManyToMany(mappedBy="traders")
private List<DataMarket> shares;
@OneToOne
private CurrencyAccount account;

public List<Option> getOptions() {
		return options;
	}

	public void setOptions(List<Option> options) {
		this.options = options;
	}


public boolean isActive() {
	return active;
}

public void setActive(boolean active) {
	this.active = active;
}

public List<DataMarket> getShares() {
	return shares;
}

public void setShares(List<DataMarket> shares) {
	this.shares = shares;
}

public CurrencyAccount getAccount() {
	return account;
}

public void setAccount(CurrencyAccount account) {
	this.account = account;
}


}
